<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = new \App\Page();
        $page->title = 'First page';
        $page->user_id = \App\User::query()->where('role_id', '=', '2')->first()->id;
        $page->save();

        $newModule = (new \App\Modules\MarkdownModule(null, $page->id));
        $newModule->setContent("# This is going to be amazing!");
    }
}
