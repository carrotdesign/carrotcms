<?php

use Illuminate\Database\Seeder;

class EssentialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Creating the required roles first:
        $roles = [
            ['name' => 'user'],
            ['name' => 'admin', 'superuser' => true],
        ];
        foreach ($roles as $role) {
            (new \App\Role($role))->save();
        }

        \App\Utils\Settings::pushString('current_theme', 'root');
    }
}
