$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.new-module-btn').click(function () {
        $(this).addClass('disabled');
        if ($(this).hasClass('disabled'))
            $.post($(this).find('a').attr('href'), {module_class: $(this).data('class')}, function (a) {
                location.reload();
            })
    })
});