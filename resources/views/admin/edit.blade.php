@extends('layouts.app')
@section('title', $page->title)
@section('head')
    {!! $head !!}
@endsection
@section('body')
    {!! $body !!}
    <script>
        $(function () {
            $('#edit-form').submit(function (e) {
                let $btn = $('#btn-save');
                $btn.html($btn.data('loading-text')).addClass('disabled');
                e.preventDefault();
                $.ajax({
                    url: '{{route('admin.edit.save', $page->id)}}',
                    method: "POST",
                    data: $(this).serialize(),
                    success: function (d) {
                        console.log(d);
                        setInterval(function () {
                            var $btn = $('#btn-save');
                            $btn.html($btn.data('normal')).removeClass('disabled');
                        }, 500)
                    }
                })
            })
        })
    </script>
@endsection
@section('content')
    <div class="container-fluid">
        <form method="post" id="edit-form">
            <div class="row">
                <div class="col-lg-8">
                    <div class="accordion" id="accordionExample">

                        @foreach($modules as $module)
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link d-block w-100" type="button" data-toggle="collapse"
                                                data-target="#collapse-{{$module->getId()}}"
                                                aria-expanded="true"
                                                aria-controls="collapse-{{$module->getId()}}">
                                            {{$module::$name}}
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapse-{{$module->getId()}}"
                                     class="collapse {{array_first($modules)->getId() === $module->getId() ? 'show' : ''}}"
                                     aria-labelledby="headingOne"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        {!! $module->getEditor() !!}
                                    </div>
                                    <div class="card-footer text-right">
                                        <a href="{{route('admin.edit.module.delete', $module->getId())}}"
                                           class="card-link text-danger">{{__('Delete')}}</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <hr>
                    @include('admin.module_selector', ['infos'=>$infos, 'page_id'=>$page->id])
                </div>
                <div class="col-lg-4">
                    <button class="btn btn-outline-success" id="btn-save" data-normal="{{__('Save')}}"
                            data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Saving..."
                            type="submit">{{__('Save')}}</button>
                    <a class="btn btn-success" href="{{route('page', $page->id)}}">{{__('View')}}</a>
                    <input type="hidden" name="module_id" value="{{json_encode($module_ids)}}">
                </div>
                @csrf
            </div>
        </form>
    </div>
@endsection
