<div class="container">
    <div class="card">
        <div class="card-header" id="headingOne">
            <h2 class="mb-0">
                <button class="btn btn-link d-block w-100" type="button" data-toggle="collapse"
                        data-target="#collapse-new-modules"
                        aria-expanded="false"
                        aria-controls="collapse-new-modules">
                    {{__('Add new modules')}}
                </button>
            </h2>
        </div>

        <div id="collapse-new-modules"
             class="collapse"
             aria-labelledby="headingOne"
             data-parent="#accordionExample">
            <div class="card-body">
                <div class="row row-eq-height">
                    @foreach($infos as $info)
                        <div class="col-12 col-lg-6">
                            <div class="new-module-btn h-100" data-class="{{json_encode($info['class'])}}">
                                <h1>{{$info['name']}}</h1>
                                <p class="lead">{{$info['description']}}</p>
                                <a class="d-none" href="{{route('admin.edit.module.new', $page_id)}}"></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="card-footer text-right">
                <a href="#"
                   class="card-link text-info">{{__('More info')}}</a>
            </div>
        </div>
    </div>
</div>