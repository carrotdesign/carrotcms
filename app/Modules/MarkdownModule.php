<?php
/**
 * Created by PhpStorm.
 * User: danie
 * Date: 18-2-2019
 * Time: 19:09
 */

namespace App\Modules;


use App\Utils\Assets\AssetFormat;
use GrahamCampbell\Markdown\Facades\Markdown;

class MarkdownModule extends PageModule implements iModule
{
    public static $name = 'Markdown Module';
    public static $description = 'The markdown module uses a simple markdown editor.';

    public static function enqueueScripts()
    {
        return [AssetFormat::asScript(asset('js/frontend.js'))];
    }

    public static function enqueueStylesheets()
    {
        return [];
    }

    public function _enqueueEditor($editor_id)
    {
        // TODO: Only require a url (with some kind of function)
        return ['scripts' => [AssetFormat::asScript('https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js'),
            "<script>
$(function(){
        var simplemde = new SimpleMDE({ spellChecker: false,element: $('#$editor_id')[0]});
        $('#edit-form').submit(function(e) {
            $('#content-$editor_id').val(simplemde.value());
        })
})
                </script>"],
            'stylesheets' => [AssetFormat::asLink('https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css')]
        ];
    }

    public function getHtmlContent()
    {
        return Markdown::convertToHtml($this->module->content);
    }

    public function _getEditor($editor_id, $field_name, $content)
    {
        return "<textarea id=\"$editor_id\">$content</textarea>
                <input type=\"hidden\" name=\"$field_name\" id='content-$editor_id'>";
    }
}