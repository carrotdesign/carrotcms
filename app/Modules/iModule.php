<?php
/**
 * Created by PhpStorm.
 * User: danie
 * Date: 20-2-2019
 * Time: 17:41
 */

namespace App\Modules;


interface iModule
{
    public function _getEditor($editor_id, $field_name, $content);
    public function _enqueueEditor($editor_id);
    public function getHtmlContent();
    public static function enqueueStylesheets();
    public static function enqueueScripts();
}