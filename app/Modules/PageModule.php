<?php
/**
 * Created by PhpStorm.
 * User: danie
 * Date: 20-2-2019
 * Time: 18:01
 */

namespace App\Modules;


use App\Models\Module;

abstract class PageModule
{
    public static $name;
    public static $description;

    private $class = 'App\Modules\PageModule';
    public $module;

    public static function createOrLoad($class, $page_id = null, $_module = null)
    {
        $module = new $class();
        $module->class = $class;
        if ($_module !== null) {
            $module->module = $_module;
        } else {
            $module->module = new Module();
            $module->module->class = $class;
            $module->module->page_id = $page_id;
            $module->module->content = '';
            $module->module->save();
        }
        return $module;
    }

    private function _getEditorId()
    {
        return 'content_input-' . $this->getId();
    }

    public function getEditorFieldName()
    {
        return $this->_getEditorId();
    }

    public function enqueueEditor()
    {
        $editor_id = $this->_getEditorId();
        return $this->_enqueueEditor($editor_id);
    }

    public static function getModule($id)
    {
        $module = Module::find($id);
        $class = $module->class;
        return PageModule::createOrLoad($class);
    }

    public function setContent($value)
    {
        $this->module->content = $value;
        $this->module->save();
    }

    public function getContent()
    {
        return $this->module->content;
    }

    public function getEditor()
    {
        $editor_id = $this->_getEditorId();
        $field_name = $this->getEditorFieldName($editor_id);
        return $this->_getEditor($editor_id, $field_name, $this->getContent());
    }

    public function getId()
    {
        return $this->module->id;
    }
}