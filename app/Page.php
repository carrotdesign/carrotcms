<?php

namespace App;

use App\Modules\PageModule;
use App\Models\Module;
use App\Utils\Assets\AssetHandler;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Page extends Model
{
    public function getModuleIds()
    {
        return DB::table('modules')
            ->select(['id'])
            ->where('page_id', '=', $this->id)
            ->get();
    }

    public function getModules($ids = null)
    {
        $modules = [];
        foreach (($ids !== null ? $ids : $this->getModuleIds()) as $moduleId) {
            $m = Module::find($moduleId->id);
            $class = $m->class;
            $module = PageModule::createOrLoad($class, null, $m);
            $modules[] = $module;
        }
        return $modules;
    }

    public function enqueueRaw($editor = false, $part = null)
    {
        // TODO: Caching & optimize
        $head = AssetHandler::$stylesheets;
        $body = AssetHandler::$scripts;
        if ($editor) {
            foreach ($this->getModules() as $module) {
                $queue = $module->enqueueEditor();
                if (isset($queue)) {
                    $head = array_merge($head, $queue['stylesheets']);
                    $body = array_merge($body, $queue['scripts']);
                }
            }
        }

        $headRaw = join("", $head);
        $bodyRaw = join("", $body);
        if ($part === 'head')
            return $headRaw;
        elseif ($part === 'body')
            return $bodyRaw;
        return ['head' => $headRaw, 'body' => $bodyRaw];
    }

    public function headQueue()
    {
        return $this->enqueueRaw(false, 'head');
    }

    public function bodyQueue()
    {
        return $this->enqueueRaw(false, 'body');
    }
}
