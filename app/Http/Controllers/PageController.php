<?php

namespace App\Http\Controllers;

use App\Modules\PageModule;
use App\Page;
use App\Utils\Settings;
use Illuminate\Http\Request;
use App\Models\Module;

class PageController extends Controller
{
    public function index()
    {
        $homepage = Settings::getInt('home', 1);
        $page = Page::find($homepage);
        $modules = [];
        foreach ($page->getModuleIds() as $moduleId) {
            $m = Module::find($moduleId->id);
            $class = $m->class;
            $module = PageModule::createOrLoad($class, null, $m);
            $modules[] = $module;
        }

        return view('theme::page', ['modules' => $modules, 'page' => $page]);
    }
}
