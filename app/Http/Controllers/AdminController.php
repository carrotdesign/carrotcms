<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Modules\PageModule;
use App\Modules\MarkdownModule;
use App\Page;
use App\Utils\ModuleUtil;
use App\Utils\Settings;
use App\Utils\ThemeUtil;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return json_encode(ThemeUtil::getCurrentThemeInfo());
    }

    public function editPage($id)
    {
        $page = Page::findOrFail($id);
        $ids = $page->getModuleIds();
        $modules = $page->getModules($ids);
        $enqueues = $page->enqueueRaw(true);
        return view('admin.edit', ['head' => $enqueues['head'], 'body' => $enqueues['body'],
            'modules' => $modules, 'page' => $page, 'module_ids' => $ids, 'infos' => ModuleUtil::getModuleInfos()]);
    }

    public function editPagePost(Request $request, $id)
    {
        $modules = null;
        try {
            $page = Page::findOrFail($id);

            $modules = $page->getModules();
            foreach ($modules as $module) {
                $name = $module->getEditorFieldName();
                $id = $module->module->id;
                if ($request->has($name)) {
                    $module->setContent($request->post($name));
                }
            }
        } catch (\Exception $exception) {
            return json_encode($exception);
        }
        return json_encode(true);
    }

    public function newModule(Request $request, $id)
    {
        $class = $request->has('module_class') ? json_decode($request->module_class) : null;
        if (!empty($class) && class_exists($class)) {
            PageModule::createOrLoad($class, $id);
            return json_encode(true);
        } else {
            return abort(500, "This module or class doesn't exist.");
        }
    }

    public function removeModule(Request $request, $id)
    {
        $module = Module::find($id);
        $page = $module->page_id;
        try {
            $module->delete();
        } catch (\Exception $e) {
            return json_encode($e);
        }
        return redirect(route('admin.edit', $page));
    }
}
