<?php

namespace App;

use App\Utils\Settings;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * @return Role | bool
     */
    public static function defaultRole()
    {
        $defaultRoleId = Settings::getInt('default_role_id', 1);
        $role = Role::find($defaultRoleId);
        return $role !== null ? $role : false;
    }
    public function users(){
        return $this->hasMany('App\User');
    }
}
