<?php
/**
 * Created by PhpStorm.
 * User: danie
 * Date: 21-2-2019
 * Time: 16:41
 */

namespace App\Utils;


use function Composer\Autoload\includeFile;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class PluginUtil
{
    public static function loadPluginFiles()
    {
        $pluginFiles = [];
        $cache = Cache::get('PLUGIN_FILES');
        if ($cache !== null) {
            foreach (json_decode($cache) as $item) {
                includeFile($item);
            }
            return;
        }

        $storage = Storage::disk('local');
        foreach ($storage->allDirectories('plugins') as $dir) {
            foreach ($storage->allFiles($dir) as $file) {
                $pluginFiles[] = $a = storage_path('app/' . $file);
                includeFile($a);
            }
        }
        Cache::put('PLUGIN_FILES', json_encode($pluginFiles), 1);
    }
}