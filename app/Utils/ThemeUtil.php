<?php
/**
 * Created by PhpStorm.
 * User: danie
 * Date: 21-2-2019
 * Time: 10:42
 */

namespace App\Utils;


class ThemeUtil
{
    public static function theme_path($path)
    {
        return storage_path('app' . DIRECTORY_SEPARATOR . 'themes' . DIRECTORY_SEPARATOR . $path);
    }

    public static function current_theme_path($file = '')
    {
        $currentTheme = Settings::getString(Settings::CURRENT_THEME, 'root');
        return self::theme_path($currentTheme) . DIRECTORY_SEPARATOR . $file;
    }

    public static function getCurrentThemeInfo()
    {
        return include_once self::current_theme_path('theme.php');
    }
}