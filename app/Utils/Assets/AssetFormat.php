<?php
/**
 * Created by PhpStorm.
 * User: danie
 * Date: 20-2-2019
 * Time: 19:27
 */

namespace App\Utils\Assets;


class AssetFormat
{
    public static function asLink($url, $attr = [])
    {
        $extra = '';
        if (count($attr) > 0) {
            foreach ($attr as $key => $value) {
                $extra .= "$key=" . "\"$value\" ";
            }
        }
        return "<link rel=\"stylesheet\" href=\"$url\" $extra>";
    }

    public static function asScript($src, $code = '')
    {

        return "<script " . (!empty($src) ? "src=\"$src\"" : '') . ">$code</script>";
    }

}