<?php
/**
 * Created by PhpStorm.
 * User: danie
 * Date: 2/20/2019
 * Time: 13:47
 */

namespace App\Utils;


class ModuleUtil
{
    public static function getModules()
    {
        return config('modules.enabled');
    }

    public static function getModuleInfos()
    {
        $m = [];
        foreach (self::getModules() as $module) {
            $m[] = self::getModuleInfo($module);
        }
        return $m;
    }

    public static function getModuleInfo($class)
    {
        return [
            'name' => $class::$name,
            'description' => $class::$description,
            'class' => $class,
        ];
    }
}