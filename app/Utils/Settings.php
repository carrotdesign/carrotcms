<?php
/**
 * Created by PhpStorm.
 * User: danie
 * Date: 19-2-2019
 * Time: 19:23
 */

namespace App\Utils;


use App\Setting;

class Settings
{
    const CURRENT_THEME = 'current_theme';
    /**
     * @param string $name
     * @param string $value
     * @return bool
     */
    public static function pushString($name, $value)
    {
        return self::pushVal($name, $value, 'string');
    }

    /**
     * @param string $name
     * @param string $value
     * @return bool
     */
    public static function pushInt($name, $value)
    {
        return self::pushVal($name, $value, 'int');
    }

    /**
     * @param string $name
     * @param string $value
     * @return bool
     */
    public static function pushBool($name, $value)
    {
        return self::pushVal($name, $value, 'int');
    }


    /**
     * @param string $name
     * @param string $default
     * @return string
     */
    public static function getString($name, $default = '')
    {
        $str = strval(self::getVal($name, 'string'));
        if ($default !== '')
            return !empty($str) ? $str : $default;
        return $str !== null ? $str : $default;

    }


    /**
     * @param string $name
     * @param int $default
     * @return int|bool
     */
    public static function getInt($name, $default = 0)
    {
        $int = self::getVal($name, 'int');
        return $int !== null ? intval($int) : $default;
    }

    /**
     * @param string $name
     * @param bool $default
     * @return bool
     */
    public static function getBool($name, $default = false)
    {
        $bool = self::getVal($name, 'bool');
        return $bool !== null ? boolval($bool) : $default;
    }

    private static function pushVal($name, $value, $type)
    {
        return (new Setting(['name' => $name, 'value' => $value, 'type' => $type]))->save();
    }

    private static function getVal($name, $type)
    {
        $setting = Setting::find($name);
        if ($setting !== null && strtolower($type) === strtolower($setting->type)) {
            return $setting->value;
        }
        return null;
    }
}