<?php

namespace App\Providers;

use App\Utils\Assets\AssetHandler;
use App\Utils\ModuleUtil;
use Illuminate\Support\ServiceProvider;

class QueueProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // TODO: Make sure that the module's script & stylesheets get lazy loaded.
        foreach (ModuleUtil::getModules() as $class) {
            AssetHandler::$scripts = array_merge(AssetHandler::$scripts, $class::enqueueScripts());
            AssetHandler::$stylesheets = array_merge(AssetHandler::$stylesheets, $class::enqueueStylesheets());
        }
    }
}
