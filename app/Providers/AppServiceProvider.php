<?php

namespace App\Providers;

use App\Utils\PluginUtil;
use App\Utils\Settings;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
//        $this->loadViewsFrom(storage_path(), 'custom_name');

        $currentThemePath = storage_path('app\themes' . DIRECTORY_SEPARATOR
            . Settings::getString('current_theme', 'root'));

        view()->addNamespace('theme', $currentThemePath);
        PluginUtil::loadPluginFiles();
    }
}
