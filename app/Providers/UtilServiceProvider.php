<?php
/**
 * Created by PhpStorm.
 * User: danie
 * Date: 2/20/2019
 * Time: 13:53
 */

namespace App\Providers;


use App\Utils\ModuleUtil;
use Illuminate\Support\ServiceProvider;

class UtilServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('ModuleUtil', function () {
            return new ModuleUtil();
        });
    }
}