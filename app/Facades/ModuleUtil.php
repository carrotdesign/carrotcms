<?php
/**
 * Created by PhpStorm.
 * User: danie
 * Date: 2/20/2019
 * Time: 13:52
 */

namespace App\Facades;
use Illuminate\Support\Facades\Facade;
class ModuleUtil extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ModuleUtil';
    }
}