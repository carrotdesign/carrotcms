<?php
return [
    'enabled' => [
        \App\Modules\MarkdownModule::class,
        \App\Modules\HtmlModule::class,
    ]
];