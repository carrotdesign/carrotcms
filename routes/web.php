<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'PageController@index');
Route::get('/page/{id}', 'PageController@index')->name('page');

Route::prefix('admin')->group(function () {
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::get('/edit/{id}', 'AdminController@editPage')->name('admin.edit');
    Route::post('/edit/{id}', 'AdminController@editPagePost')->name('admin.edit.save');
    Route::post('/edit/{id}/module', 'AdminController@newModule')->name('admin.edit.module.new');
    Route::get('/edit/module/{id}/delete', 'AdminController@removeModule')->name('admin.edit.module.delete');
});

Auth::routes();